using Fungus;

public class Deccission
{
    public enum Possibilities
    {
        None = 0,
        Union = 1,
        GiveUp = 2,
        SelfSacrifice = 3,
        OtherSacrifice = 4,
    };

    public string character;
    public Possibilities decission = Possibilities.None;
    public bool convinced = false;
    public bool sacrificeAngustias = false;
    public bool sacrificeToby = false;
    public bool sacrificeCristina = false;
    public bool sacrificeCharlie = false;

    public Deccission(string character, Flowchart flowchart) {
        this.character = character;
        string shortChar = this.character.Substring(0, 3).ToLower();
        this.convinced = flowchart.GetBooleanVariable(shortChar+"Conv");
        if (this.convinced) {
            bool union = flowchart.GetBooleanVariable(shortChar + "Unirse");
            bool giveUp = flowchart.GetBooleanVariable(shortChar + "Rendirse");
            bool selfSacrifice = flowchart.GetBooleanVariable(shortChar + "Sacrificarse");
            bool otherSacrifice = flowchart.GetBooleanVariable(shortChar + "SacrificarOtro");
            if (union) this.decission = Possibilities.Union;
            if (giveUp) this.decission = Possibilities.GiveUp;
            if (selfSacrifice) this.decission = Possibilities.SelfSacrifice;
            if (otherSacrifice){
                this.decission = Possibilities.OtherSacrifice;
                if(shortChar!="ang") this.sacrificeAngustias = flowchart.GetBooleanVariable(shortChar + "SacrificarAng");
                if (shortChar != "tob") this.sacrificeToby = flowchart.GetBooleanVariable(shortChar + "SacrificarTob");
                if (shortChar != "cri") this.sacrificeCristina = flowchart.GetBooleanVariable(shortChar + "SacrificarCri");
                if (shortChar != "cha") this.sacrificeCharlie = flowchart.GetBooleanVariable(shortChar + "SacrificarCha");

            }
        }
    }
}
