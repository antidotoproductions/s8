using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

/// <summary>
/// MMC
/// Gestiona los textos y las im?genes de cada final. 
/// </summary>
/// 

#region Variables

public class FinalManager : MonoBehaviour
{
    // Textos de los finales
    public string strFinal1;
    public string strFinal2;
    public string strFinal3;
    public string strFinal4;
    public string strFinal5;
    public string strFinal6;
    public string strFinal7;
    public string strFinal8;
    private string strFinal;

    public Text txtFinal;

    // Imagenes de las finales
    public Sprite sprFinal1;
    public Sprite sprFinal2;
    public Sprite sprFinal3;
    public Sprite sprFinal4;
    public Sprite sprFinal5;
    public Sprite sprFinal6;
    public Sprite sprFinal7;
    public Sprite sprFinal8;
    public Image imgFinal;

    //Fade In
    public GameObject goFadeIN;
    public Animator animFade;

    //Botones Volver al men?/Salir
    public GameObject goButtons;

    //Cr�ditos
    public GameObject goCreditsTxt;

    public GameObject goFinalScreen;
    public GameObject goFinalText;

    /// <summary>
    /// ANDRO
    /// Gestiona las decisiones que se han tomado y el n?mero de personajes que se alinean con cada decisi?n.
    /// </summary>
    ///

    //Variables de selecci?n de final
    public Flowchart flowchart;
    public string[] personajes;
    private List<Deccission> deccissions;
    public int[] votes = { 0, 0, 0, 0 }; //Unirse, Rendirse, Sacrificarse, Sacrificar Otro
    public int[] sacrificeVotes = { 0, 0, 0, 0 }; //Angustias, Toby, Cristina, Charlie
    public int indexFinal = 0;
    public bool final = false;
    public bool justIsfinal = false;
    public bool justConvinced = false;


    public Image finalAng;
    public Image finalTob;
    public Image finalCri;
    public Image finalCha;


    bool enableAng;
    bool enableTob;
    bool enableCri;
    bool enableCha;

    #endregion Variables

    #region Funciones de Unity

    private void Update(){
        int invFloor = -flowchart.GetIntegerVariable("floor");
        bool allConvinced = CheckIfAllConvinced();
        final = flowchart.GetBooleanVariable("final");
        if (allConvinced && !justConvinced){
            justConvinced = true;
            flowchart.ExecuteBlock("AllConvinced");
        }
        if (final && !justIsfinal)
        {
            justIsfinal = true;
            this.GetDeccissions();
            this.ProcessDeccissions();
            this.CountVotes();
        }
    }


    #endregion Funciones de Unity

    #region Funciones propias

    public void PantallaFinal(int iFinal){


        switch (iFinal)
        {
            case 0:
                strFinal = strFinal1;
                imgFinal.sprite = sprFinal1;
                finalAng.enabled = true;
                finalTob.enabled = true;
                finalCri.enabled = true;
                finalCha.enabled = true;
                break;
            case 1:
                strFinal = strFinal2;
                imgFinal.sprite = sprFinal2;
                finalAng.enabled = false;
                finalTob.enabled = false;
                finalCri.enabled = false;
                finalCha.enabled = false;
                break;
            case 2:
                strFinal = strFinal3;
                imgFinal.sprite = sprFinal3;
                finalAng.enabled = false;
                finalTob.enabled = false;
                finalCri.enabled = false;
                finalCha.enabled = true;
                break;
            case 3:
                strFinal = strFinal4;
                imgFinal.sprite = sprFinal4;
                finalAng.enabled = true;
                finalTob.enabled = false;
                finalCri.enabled = false;
                finalCha.enabled = false;
                break;
            case 4:
                strFinal = strFinal5;
                imgFinal.sprite = sprFinal5;
                finalAng.enabled = false;
                finalTob.enabled = true;
                finalCri.enabled = false;
                finalCha.enabled = false;
                break;
            case 5:
                strFinal = strFinal6;
                imgFinal.sprite = sprFinal6;
                finalAng.enabled = false;
                finalTob.enabled = false;
                finalCri.enabled = true;
                finalCha.enabled = false;
                break;
            case 6:
                strFinal = strFinal7;
                imgFinal.sprite = sprFinal7;
                finalAng.enabled = true;
                finalTob.enabled = true;
                finalCri.enabled = true;
                finalCha.enabled = true;
                break;
            case 7:
                strFinal = strFinal8;
                imgFinal.sprite = sprFinal8;
                finalAng.enabled = false;
                finalTob.enabled = false;
                finalCri.enabled = false;
                finalCha.enabled = false;
                break;
        }
                StartCoroutine (FillTheBox(strFinal));
    }


    IEnumerator FillTheBox(string strText)
    {
        goFadeIN.SetActive(true);
        animFade.SetBool("FadeIn", true);
        goFinalScreen.SetActive(true);
        yield return new WaitForSeconds(3);
        goFinalText.SetActive(true);
        txtFinal.text = strText;
        yield return new WaitForSeconds(5);
        goFadeIN.SetActive(false);
        goCreditsTxt.SetActive(true);
        goButtons.SetActive(true);
        
    }


    /// <summary>
    /// ANDRO
    /// Gestiona las decisiones que se han tomado y el n?mero de personajes que se alinean con cada decisi?n.
    /// </summary>
    ///

    private bool CheckIfAllConvinced()
    {
        bool angConv = flowchart.GetBooleanVariable("angConv");
        bool tobConv = flowchart.GetBooleanVariable("tobConv");
        bool criConv = flowchart.GetBooleanVariable("criConv");
        bool chaConv = flowchart.GetBooleanVariable("chaConv");
        return angConv && tobConv && criConv && chaConv;
    }

    private void GetDeccissions()
    {
        deccissions = new List<Deccission>();
        foreach (string p in this.personajes)
        {
            Deccission dec = new Deccission(p, flowchart);
            deccissions.Add(dec);
        }
    }

    private void ProcessDeccissions()
    {
        foreach (Deccission dec in this.deccissions)
        {
            if (dec.decission == Deccission.Possibilities.Union) votes[0]++;
            if (dec.decission == Deccission.Possibilities.GiveUp) votes[1]++;
            if (dec.decission == Deccission.Possibilities.SelfSacrifice) votes[2]++;
            if (dec.decission == Deccission.Possibilities.OtherSacrifice)
            {
                votes[3]++;
                if (dec.sacrificeAngustias) sacrificeVotes[0]++;
                if (dec.sacrificeToby) sacrificeVotes[1]++;
                if (dec.sacrificeCristina) sacrificeVotes[2]++;
                if (dec.sacrificeCharlie) sacrificeVotes[3]++;
            }
        }
    }

    private void CountVotes()
    {
        bool finalFound = false;
        if (votes[2] > 0){
            PantallaFinal(2);
            finalFound = true;
        }
        else{
            //Unirse, Rendirse, Sacrificarse, Sacrificar Otro
            if (votes[0] == 4)
            {
                finalFound = true;
                PantallaFinal(0);
            }
            else if (votes[0] > votes[1] && votes[0] > votes[3])
            {
                finalFound = true;
                PantallaFinal(1);
            }
            else if (votes[1] > votes[0] && votes[1] > votes[3])
            {
                finalFound = true;
                PantallaFinal(6);
            }
            else if (votes[3] > votes[0] && votes[3] > votes[1])
            {
                //Angustias, Toby, Cristina, Charlie
                if (sacrificeVotes[0] == 2) {
                    PantallaFinal(3);
                    finalFound = true;
                }
                if (sacrificeVotes[1] == 2) {
                    PantallaFinal(4);
                    finalFound = true;
                }
                if (sacrificeVotes[2] == 2) {
                    PantallaFinal(5);
                    finalFound = true;
                }
                if (sacrificeVotes[3] == 2) {
                    PantallaFinal(2);
                    finalFound = true;
                }
            }

        }
        if (!finalFound) PantallaFinal(7);
    }
    #endregion Funciones propias
}





