using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{

    public GameObject goFadeIN;
    public Animator animFade;
    public GameObject goIntroText;

    public void Start()
    {
        StartCoroutine(FillTheBox());
    }



    IEnumerator FillTheBox()
    {
        goFadeIN.SetActive(true);
        animFade.SetBool("FadeIn", true);
        yield return new WaitForSeconds(3);
        goIntroText.SetActive(true);        
        yield return new WaitForSeconds(5);
        

    }
}
