using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class IntroPressKey : MonoBehaviour
{
    public GameObject goFadePanel;
    public Animator fadeAnima;


    private void Update()
    {
        AnyKey();
    }


    private void AnyKey()
    {
        if (Keyboard.current.anyKey.IsPressed())//|| Mouse.current.leftButton.isPressed||Gamepad.current.IsPressed())
        {
            StartCoroutine(StartGame());
        }

        if (Mouse.current.leftButton.isPressed)
        {
            StartCoroutine(StartGame());
        }

    IEnumerator StartGame()
    {
        fadeAnima.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        // La primera escena debe estar Indexada inmediatamente despu�s del men�.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    }
}
