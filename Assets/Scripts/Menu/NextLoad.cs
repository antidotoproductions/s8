using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Fungus;

public class NextLoad : MonoBehaviour
{
    /// <summary>
    /// MMC
    /// Carga la primera escena, sale del juego o vuelve al men? principal.
    /// </summary>
    /// 

    #region Variables

    public Animator fadeAnima;
    //public Animator[] btnAnima;
    public AudioSource btnPressed;
    public GameObject goFadePanel;
    public int iIndex;

    #endregion Variables


    #region Funciones propias
    public void StartGame()
    {
        StartCoroutine(FadeAndLoadNewGame());
    }

    public void ExitGame()
    {
        StartCoroutine(ExitGameRoutine());
    }
    public void ExitGameFinal()
    {
        StartCoroutine(ExitGameEnd());
    }

    public void Back2Intro()
    {
        StartCoroutine(Back2I());
    }

    public void Back2MainMenu()
    {
        StartCoroutine(Back2MM());
    }

    // Carga la animaci?n del bot?n correspondiente, toca un sonido, hace fade out y carga la primera scene
    IEnumerator FadeAndLoadNewGame()
    {
        //iDummy nos indica en qu? bot?n estamos, para seleccionar su animaci?n.
        iIndex = btnSelection.iDummy;
        //btnAnima[iIndex].SetBool("Pressed", true);
        btnPressed.Play(0);
        goFadePanel.SetActive(true);
        fadeAnima.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        // La primera escena debe estar Indexada inmediatamente despu?s del men?.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    IEnumerator Back2I()
    {
        //El bot?n para volver es el 0.
        iIndex = btnSelection.iDummy;
        //btnAnima[0].SetBool("Pressed", true);
        btnPressed.Play(0);
        goFadePanel.SetActive(true);
        fadeAnima.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        // el men? pricipal es el 0.
        SceneManager.LoadScene(1);
    }
    IEnumerator Back2MM()
    {
        //El bot?n para volver es el 0.
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }

        

        btnPressed.Play(0);
        goFadePanel.SetActive(true);
        fadeAnima.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        // el men? pricipal es el 0.
        SceneManager.LoadScene(0);
    }


    // Carga la animaci?n del bot?n correspondiente, toca un sonido, hace fade out y sale de la aplicaci?n
    IEnumerator ExitGameRoutine()
    {
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        iIndex = -1;
        //iDummy nos indica en qu? bot?n estamos, para seleccionar su animaci?n.
        iIndex = btnSelection.iDummy;
        //Si vengo del men? final...
        if (iIndex == -1) iIndex = 1;
        //btnAnima[iIndex].SetBool("Pressed", true);
        btnPressed.Play(0);
        goFadePanel.SetActive(true);
        fadeAnima.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        Application.Quit();
        Debug.Log("Salgo");
    }
    IEnumerator ExitGameEnd()
    {
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }

        
        //Hardcodeo el indice para este caso.
        //btnAnima[1].SetBool("Pressed", true);
        btnPressed.Play(0);
        goFadePanel.SetActive(true);
        fadeAnima.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        Application.Quit();
        Debug.Log("Salgo");
    }



    #endregion Funciones propias
}