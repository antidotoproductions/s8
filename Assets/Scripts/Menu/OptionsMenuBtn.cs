using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// MMC
/// Esta funci�n tan s�lo establece la selecci�n en un bot�n del men� de guardado cuando se viene del men� principal. Si no se
/// hace, se pierde el foco y no se puede navegar con teclado o gamepad.
/// </summary>

public class OptionsMenuBtn : MonoBehaviour
{
    #region Variables

    public Button btnOptionsDefault;


    #endregion Variables

    #region Funciones de Unity
    public void SelectButton()
    {
        btnOptionsDefault.Select();
    }

    #endregion Funciones de Unity
}