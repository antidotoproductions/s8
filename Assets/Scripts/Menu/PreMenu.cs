using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PreMenu : MonoBehaviour
{

    public GameObject goPreMenuTimeline;
    public GameObject goPressAnyKey;
    public GameObject goMenus;
    public GameObject goDefaultBtnSelection;
    //public GameObject goMenuMusic;
    public GameObject goMyself;
    public GameObject goTitulo;
    public GameObject goBtnFirstMain;
    



    void Update()
    {
        AnyKey();
    }

    private void AnyKey()
    {
        if (Keyboard.current.anyKey.IsPressed())//|| Mouse.current.leftButton.isPressed||Gamepad.current.IsPressed())
        {
            goTitulo.SetActive(false);
            StartCoroutine(Open2Menus());
        }

        if (Mouse.current.leftButton.isPressed)
        {
            goTitulo.SetActive(false);
            StartCoroutine(Open2Menus());
        }

    }


    IEnumerator Open2Menus()
    {
        goPressAnyKey.SetActive(false);
        goPreMenuTimeline.SetActive(true);
        goMenus.SetActive(true);
        goDefaultBtnSelection.SetActive(true);
        //EventSystem.current.SetSelectedGameObject(goBtnFirstMain);
        yield return new WaitForSeconds(2f);
        //goMenuMusic.SetActive(true);
        goMyself.SetActive(false);

    }
}
