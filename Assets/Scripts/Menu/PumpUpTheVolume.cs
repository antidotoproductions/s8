﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script que gestiona el volumen con playerprefs. Ver PumpUpTheBrightness
/// </summary>

#region Variables


public class PumpUpTheVolume : MonoBehaviour
{
    public Slider volumeSlider;
    public float fVolume;
    public Image muted;

#endregion Variables

#region Funciones  de Unity
    private void Start()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("audioVolume", 0.5f);
        AudioListener.volume = volumeSlider.value;
        AmIMuted();
            
    }

#endregion Funciones de Unity

#region Funciones propias

    public void SliderChange(float _value)
    {
        fVolume = _value;
        PlayerPrefs.SetFloat("audioVolume", fVolume);
        AudioListener.volume = volumeSlider.value;
        AmIMuted();
    }

    public void AmIMuted()
    {
        if (volumeSlider.value == 0)
        {
            muted.enabled = true;
        }
        else
        {
            muted.enabled = false;
        }
    }
#endregion Funciones propias
}
