using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class btnSelection : MonoBehaviour
{

    /// <summary>
    /// MMC.
    /// Recorro los botones para ver cu�l est� seleccionado. Si corresponde, aplico la animaci�n y el sonido.
    /// </summary>

    #region Variables

    // Variables para recorrer los botones 

    public int iMaxBtnMain;
    public int iMaxBtnLoader;    
    public int iMaxBtnLogros;    
    private int iMaxBtnPanel;    
    public Button[] btnMainMenu;
    public Button[] btnOptions;
    public Button[] btnLogros;
    private int i;
    public static int iDummy = -1; // Le doy un valor de -1 porque as� no coincidir� nunca en la primera pasada con el rango de botones
                             // de los paneles

    // Variables de animaci�n y sonido
    private Animator btnAnimator;
    public AudioClip sndSelected;
    private AudioSource myAudioSource;
    private bool bAlreadyPlayed;
    static bool bAmIinMain;
    static bool bAmIinOptions;
    static bool bAmIinLogros;
    static bool bAmIinPreMenu;
    private bool bButtonSelected;

    // Variables para saber en qu� men� estoy.
    public GameObject goMenuPpal;
    public GameObject goMenuOptions;
    public GameObject goMenuLogros;
    public GameObject goPreMenu;

    // Variables para seleccionar el primer elemento si cambio de paneles
    public GameObject goBtnFirstMain;
    public GameObject goBtnFirstOptions;
    public GameObject goBtnFirstLogros;
    

    #endregion Variables

    #region Funciones de Unity
    
    private void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        
    }

    private void Update()
    {
        WhereAmI();
        btnSelected();
    }

    #endregion Funciones de Unity

    #region Funciones propias

    // Determina en que panel estoy (Main o Loader) y resetea el dummy.
    private void WhereAmI()
    {

        if (goMenuPpal.activeInHierarchy==true)
        {
            // Si vengo de otro panel, reseteo el valor de iDummy para recorrer la tabla bien. Tambi�n selecciono el bot�n de 
            // btnStart para no perder el foco.
            if (!bAmIinMain)
            {
                iDummy = -1;
                //EventSystem.current.SetSelectedGameObject(goBtnFirstMain);
            }
            bAmIinMain = true;
            bAmIinOptions = false;
            bAmIinLogros = false;
            bAmIinPreMenu = false;
        }
        else if(goMenuOptions.activeInHierarchy==true)
        {

            // Si vengo de otro panel, reseteo el valor de iDummy para recorrer la tabla bien.Tambi�n selecciono el bot�n de 
            // btnAtras para no perder el foco.
            if (bAmIinOptions)
            {
                iDummy = -1;                
            }
            bAmIinMain = false;
            bAmIinOptions = true;
            bAmIinLogros = false;
            bAmIinPreMenu = false;
        }
        else if(goMenuLogros.activeInHierarchy==true)
        {

            // Si vengo de otro panel, reseteo el valor de iDummy para recorrer la tabla bien.Tambi�n selecciono el bot�n de 
            // btnAtras para no perder el foco.
            if (bAmIinLogros)
            {
                iDummy = -1;                
            }
            bAmIinMain = false;
            bAmIinOptions = false;
            bAmIinLogros = true;
            bAmIinPreMenu = false;
        }
        else if (goPreMenu.activeInHierarchy == true)
        {
            iDummy = -1;
            bAmIinMain = false;
            bAmIinOptions = false;
            bAmIinLogros = false;
            bAmIinPreMenu = true;
        }

    }

    // Dependiendo de si estoy en el men� principal o en el de cargado, recorro todos los botones que hay y si me muevo de uno a otro
    // activo una animaci�n y un sonido
    private void btnSelected()
    {
        if (!bAmIinPreMenu)
        {

            if (bAmIinMain)
            {
                iMaxBtnPanel = iMaxBtnMain;
            }
            else if (bAmIinOptions)
            {
                iMaxBtnPanel = iMaxBtnLoader;
            }
            else if (bAmIinLogros)
            {
                iMaxBtnPanel = iMaxBtnLogros;
            }

            for (i = 0; i <= iMaxBtnPanel; i++)
            {
                bButtonSelected = false;
                if (bAmIinMain)
                {
                    btnAnimator = btnMainMenu[i].GetComponent<Animator>();
                    if (btnMainMenu[i].gameObject == EventSystem.current.currentSelectedGameObject)
                    {
                        if (iDummy != i && iDummy != -1)
                        {
                            bAlreadyPlayed = false;
                            btnAnimator.SetBool("Selected", true);
                            PlaySound();
                            bButtonSelected = true;
                        }
                        iDummy = i;

                    }
                    else
                    {
                        btnAnimator.SetBool("Selected", false);
                    }
                }
            
                else if (bAmIinOptions)
                {
                    btnAnimator = btnOptions[i].GetComponent<Animator>();
                    if ((btnOptions[i].gameObject == EventSystem.current.currentSelectedGameObject))
                    {
                        if (iDummy != i && iDummy != -1)
                        {
                            bAlreadyPlayed = false;
                            btnAnimator.SetBool("Selected", true);
                            PlaySound();
                            bButtonSelected = true;
                        }
                        iDummy = i;
                    }                
                }

                else if (bAmIinLogros)
                {
                    btnAnimator = btnLogros[i].GetComponent<Animator>();
                    if (btnLogros[i].gameObject == EventSystem.current.currentSelectedGameObject)
                    {
                        if (iDummy != i && iDummy != -1)
                        {
                            bAlreadyPlayed = false;
                            btnAnimator.SetBool("Selected", true);
                            PlaySound();
                            bButtonSelected = true;
                        }
                        iDummy = i;

                    }
                    else
                    {
                        btnAnimator.SetBool("Selected", false);
                    }
                }
            }
        }
        

    }

    private void PlaySound()
    {
        if (!bAlreadyPlayed)
        {
            myAudioSource.PlayOneShot(sndSelected);
            bAlreadyPlayed = true;
        }
    }



    #endregion Funciones propias    
}
