using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PauseOnGame : MonoBehaviour
{

    public GameObject goPausa;

    private void Update()
    {
        PauseGame();
    }

    private void PauseGame()
    {
        if (Keyboard.current.escapeKey.isPressed)
        {
            Time.timeScale = 0;
            goPausa.SetActive(true);
        }
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        goPausa.SetActive(false);
    }

}
