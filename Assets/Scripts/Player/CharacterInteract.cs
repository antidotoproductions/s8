using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInteract : MonoBehaviour
{
    [SerializeField] private GameObject children;

    void Start()
    {
        children.SetActive(false);
    }

    private void OnMouseClick()
    {
        children.SetActive(false);
    }

    private void OnMouseEnter()
    {
        children.SetActive(true);
    }

    private void OnMouseExit()
    {
        children.SetActive(false);
    }
}
