using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDialoguesActive : MonoBehaviour
{
    [SerializeField] private GameObject sayDialog;
    [SerializeField] private GameObject introDialog;
    [SerializeField] private GameObject menuDialog;
    [SerializeField] private GameObject tobyInteract;
    [SerializeField] private GameObject angustiasInteract;
    [SerializeField] private GameObject charlieInteract;
    [SerializeField] private GameObject cristinaInteract;

    private void Update()
    {
        if(sayDialog.gameObject.activeSelf || introDialog.gameObject.activeSelf || menuDialog.gameObject.activeSelf)
        {
            tobyInteract.SetActive(false);
            angustiasInteract.SetActive(false);
            charlieInteract.SetActive(false);
            cristinaInteract.SetActive(false);
        }
    }
}
